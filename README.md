# metaG_course

Materials for the metagenomic bioinformatic course Oct 2021

This day is about taxonomic profiling and comparative metagenome analysis.

# Schedule 

| Time  | Lesson  | Title  |
|---|---|---|
| 10:00 - 11:00  | Keynote lecture  | [Prof. Dr. Shinichi Sunagawa](http://www.sunagawa.de/)  |
break
| 11:15 - 12:00  | lecture  | Taxonomic Profiling of Shotgun Metagenomes - Georg Zeller  |
| 12:00 - 13:00  | practical  | Taxonomic Profiling of Shotgun Metagenomes - Alessio Milanese |
lunch break
| 14:00 - 14:30  | practical & discussion  | Taxonomic Profiling of Shotgun Metagenomes - Alessio Milanese |
| 14:30 - 15:15  | lecture  | Comparative Metagenome Analysis - Georg Zeller & Jakob Wirbel  |
coffee break
| 15:30 - 17:00  | practical & discussion  | Comparative Metagenome Analysis - Jakob Wirbel |


# Contact

Please feel free to contact us if you have any other questions:

* alessio.milanese[at]embl.de
* jakob.wirbel[at]embl.de
* georg.zeller[at]embl.de


