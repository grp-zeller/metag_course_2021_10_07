![alt text](https://www.embl.de/download/zeller/metaG_course_2021/a.png)


# Setup

If you are in the denbi VM, open the terminal and run:
```
conda activate mgcourse
```

If you run it on your computer, you need to install the tools yourself. We suggest to use conda to install:
- mOTUs3 (`conda install -c bioconda motus on conda`)
- MetaPhlAn3 (`conda install -c bioconda metaphlan`)
- trimmomatic (`conda install -c bioconda trimmomatic`)
- FastQC (`conda install -c bioconda fastqc`)

# Practical 1: Quality control and read trimming

Objectives of the practical:
- Familiarise with fastq files, how to look at them and what they represent;
- Learn how to understand if the reads in your files are of good quality;
- Learn how to remove and trim low quality reads.

Required tools:
- [trimmomatic](http://www.usadellab.org/cms/?page=trimmomatic) (install with [conda](https://anaconda.org/bioconda/trimmomatic))
- [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) (install with [conda](https://anaconda.org/bioconda/fastqc))

Required file:
- [https://www.embl.de/download/zeller/metaG_course/fastq_files/raw_reads_1.fastq](https://www.embl.de/download/zeller/metaG_course/fastq_files/raw_reads_1.fastq) (~22 Mb)
- [https://www.embl.de/download/zeller/metaG_course/fastq_files/raw_reads_2.fastq](https://www.embl.de/download/zeller/metaG_course/fastq_files/raw_reads_2.fastq) (~22 Mb)



## Exercises

1. Open the terminal, create a directory in the `~/Desktop` and download the two required files with `wget`.
   <details><summary>SOLUTION</summary>
   <p>

   ```
   cd 
   mkdir metaG_tax
   cd metaG_tax
   wget https://www.embl.de/download/zeller/metaG_course/fastq_files/raw_reads_1.fastq
   wget https://www.embl.de/download/zeller/metaG_course/fastq_files/raw_reads_2.fastq
   ```
   </p> 

2. Look at the fastq files, how are they structured?
   <details><summary>SOLUTION</summary>
   <p>


   When we work with metagenomic data we usually have two fastq files produced by
   the Illumina sequencer:
   - a file containing the forward reads
   - a file containing the reverse reads
   
   Usually the prefix of the file name is the same, and we have `_1_` for the file
   with forward reads and `_2_` for the file with reverse reads, example:
   ```
   HYG3LBGXC_261_1_19s00-sample119s004346_1_sequence.txt.gz
   HYG3LBGXC_261_1_19s00-sample119s004346_2_sequence.txt.gz
   ```
   
   A fastq file contains 4 lines for each read, with the following information:
   
   | Line | Description |
   | ------ | ------ |
   | 1 | A line starting with `@` and the read id |
   | 2 | The DNA sequence | 
   | 3 | A line starting with `+` and sometimes the same information as in line 1 | 
   | 4 | A string of characters that represents the quality score (same number of characters as in line 2) | 
   
   We can have a look at the first read (4 lines) with `head -n 4 raw_reads_1.fastq`:
   ```
   @read1
   CTCTAGCAGATACTCTCCCTATATGAACTCATGGGGGCGGGGATGCCCGTCCTGTGTAACAATAAAAAATAACCTTGATGAGGGCGGATAGATCCTACCT
   +
   BBBFFFF<FBFFFFFFIIIIBFBFFBFIIIIFFFIIIIIFFBFFFFFB77BBFFBBBBBBBBBBFFFB7<0BBBB<BBBBFBBBFFF<<7BBBFFFBBBB

   ```

   Each character in the fourth line can be converted to a quality score ([Phred-33](https://support.illumina.com/help/BaseSpace_OLH_009008/Content/Source/Informatics/BS/QualityScoreEncoding_swBS.htm)) from 1 to 40:
   ```
        Character: !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHI
                   |         |         |         |         |
    Quality score: 0........10........20........30........40 
   ```
   
   And, for each quality score there is an associated probability for correctly calling a base:
   
   | Quality Score | Probability of incorrect base call | Base call accuracy |
   | ------ | ------ | ------ | 
   | 10 | 1 in 10 | 90% |
   | 20 | 1 in 100 | 99% |
   | 30 | 1 in 1000 | 99.9% |
   | 40 | 1 in 10,000 | 99.99% |
   
   </p> 


3. Use `fastqc` ([link](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/)) to check the quality of the reads in the two files.
   <details><summary>SOLUTION</summary>
   <p>
 
   Run on the terminal:
   ```
   fastqc raw_reads_1.fastq
   fastqc raw_reads_2.fastq
   ```
   
   We can open the [html file for the reverse reads](https://www.embl.de/download/zeller/metaG_course/pics/raw_reads_2_fastqc.html). There a bunch of graphs, but probably the most interesting is:
   
   ![alt text](https://www.embl.de/download/zeller/metaG_course/pics/average_quality_reverse_read_raw.png)
   
   This view shows an overview of the range of quality values across all bases at each position in the FastQ file. 
   For each position a BoxWhisker type plot is drawn with:
   - median as a red line,
   - the inter-quartile range (25-75%) as a yellow box,
   - upper and lower whiskers represent the 10% and 90% points,
   - the blue line represents the mean quality.
   
   The y-axis on the graph shows the quality scores. The higher the score the better the base call. The background of the graph divides the y axis into very good quality calls (green), calls of reasonable quality (orange), and calls of poor quality (red). The quality of calls on most platforms will degrade as the run progresses, so it is common to see base calls falling into the orange area towards the end of a read.
   
   </p> 
   

4. Have a look at the first four reads, which reads would you remove?
   <details><summary>SOLUTION</summary>
   <p>

   If you have a look at the first four reads with `head -n 16 raw_reads_1.fastq` and `head -n 16 raw_reads_2.fastq`, you can see that:
   - `@read1` in the forward file looks fine, while in the reverse file only 22 nucleotides have been sequenced:  
      ```
      @read1
      GCTCGATGGAGAGGGAAGGTT
      +
      BBBFFFFBFFFFFBBFFFIFF
      ```
   - `@read54` in the forward file has only 4 sequenced nucleotides and all other are N's:  
      ```
      @read54
      NATTNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
      +
      #AAA################################################################################################
      ```  
   - `@read315` in the forward file has only 2 low quality nucleotides at the end:
      ```
      @read315
      TTGTAGTGCTTGTCGAATTCGACGTAGTATTTGCCTACGAGGTGGTCGCCCTTCATGCCCGACGTGGCGGGCGTTTCGCCGTTGCCGTAGAGTTTCCANN
      +
      BBBFFFFFFFFFFIIIIIIIIIIIFFIFIIIIIIIIIIIIIIFIIIIIIIIIIIFFFFFFFBBBBFFFFFFB<0<7<B<B<<BB<B<7<<0000B<<<##
      ```
   - `@read337` is of low quality in both files.
   
   We would like to remove `@read1` reverse and `@read54` forward because they are of low quality. On the other hand, `@read315` forward looks fine, except for the last 2 nucleotides.
   Hence, we would like to trim only the low quality nucleotides at the end (or beginning) of the read.  
   For `@read337`, we would like to remove both forward and reverse.

   </p> 

5. Use `trimmomatic` ([trimmomatic](http://www.usadellab.org/cms/?page=trimmomatic)) to remove short reads (<40 bp), low quality reads and remove low quality bases at the end/beginning of the reads. What files are produced by trimmomatic?
   <details><summary>SOLUTION</summary>
   <p>

   Use the following command:
   ```bash
   trimmomatic PE raw_reads_1.fastq raw_reads_2.fastq LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:40 -baseout filtered
   ```

   Which produces 4 files:
   - `filtered_1P`, containing the forward reads that pass the filter and have a mate (in `filtered_2P`);
   - `filtered_1U`, containing the forward reads that pass the filter and do not have a mate (the paired reverse read didn't pass the filter)
   - `filtered_2P`, containing the reverse reads that pass the filter and have a mate (in `filtered_1P`);
   - `filtered_2U`, containing the reverse reads that pass the filter and do not have a mate (the paired forward read didn't pass the filter)
     
   </p> 

6. Check the filtered reverse reads with `fastqc`, is the read quality improved?
   <details><summary>SOLUTION</summary>
   <p>

   If we run again `fastQC` on the reverse paired end reads after filtering (`filtered_2P`), with:
   ```bash
   fastqc filtered_2P
   ```
   
   we obtain the following [html file](https://www.embl.de/download/zeller/metaG_course/pics/filtered_2P_fastqc.html). Now, we have better quality reads:
   
   ![alt text](https://www.embl.de/download/zeller/metaG_course/pics/compared_fastqc.png)
 

   </p> 



## References and additional information

1. [Intro to FastQC with explanation of a FASTA file](https://hbctraining.github.io/Intro-to-rnaseq-hpc-O2/lessons/02_assessing_quality.html)
2. [Per base sequence quality fastQC plot](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/Help/3%20Analysis%20Modules/2%20Per%20Base%20Sequence%20Quality.html)
3. [Trimmomatic manual](http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/TrimmomaticManual_V0.32.pdf)


























![alt text](https://www.embl.de/download/zeller/metaG_course_2021/a.png)

# Practical 2: Taxonomic profiling

Objectives of the practical:
- Learn how to create a taxonomic profile for a (quality filtered) metagenomic sample using mOTUs;
- Familiarise with the parameters and the output of a taxonomic profiling tool;
- Learn how to merge many taxonomic profile files (all the profiles from your study) into one file.

Required tools:
- [mOTUs](https://github.com/motu-tool/mOTUs_v2) (install with [conda](https://anaconda.org/bioconda/motus))
- [metaphlan](https://github.com/biobakery/MetaPhlAn/wiki/MetaPhlAn-3.0) (optional, install with [conda](https://anaconda.org/bioconda/metaphlan))

Required file:
- filtered fastq files created in the previous practical

## Taxonomic profiling

The majority of microbiome studies rely on an accurate identification of the microbes and quantification their abundance in the sample under study, a process called taxonomic profiling.

We would like to save the profile in a file like:
```
Bacteroides_vulgatus    0.34
Prevotella_copri        0.16
Eubacterium_rectale     0.10
...
```

And, if we pull together many samples:
```
                        sample_1  sample_2
Bacteroides_vulgatus        0.34      0.01
Prevotella_copri            0.16      0.42
Eubacterium_rectale         0.10      0.00
```


We use [mOTUs](https://github.com/motu-tool/mOTUs_v2) to create taxonomic profiles of metagenomic samples.
More information can be found also in [this protocol paper](https://currentprotocols.onlinelibrary.wiley.com/doi/full/10.1002/cpz1.218).

## Exercises

1. Use `motus` (manual: [link](https://github.com/motu-tool/mOTUs_v2#simple-examples)) to create a profile from the files created by trimmomatic.
   <details><summary>SOLUTION</summary>
   <p>

   You can download the 4 files that you created with `trimmomatic` in the previous practical with (in case you don't have them):
   ```
   wget https://www.embl.de/download/zeller/metaG_course/fastq_files/filtered_1P
   wget https://www.embl.de/download/zeller/metaG_course/fastq_files/filtered_1U
   wget https://www.embl.de/download/zeller/metaG_course/fastq_files/filtered_2P
   wget https://www.embl.de/download/zeller/metaG_course/fastq_files/filtered_2U
   
   ```
   
   You can profile the sample with:
   
   ```
   motus profile -f filtered_1P -r filtered_2P -s filtered_1U,filtered_2U -o test_sample.motus -t 8 -n test_sample
   ```
   
   Which produces (`head test_sample.motus`):
   ```
   # git tag version 3.0.1 |  motus version 3.0.1 | map_tax 3.0.1 | gene database: nr3.0.1 | calc_mgc 3.0.1 -y insert.scaled_counts -l 75 | calc_motu 3.0.1 -k mOTU -C no_CAMI -g 3 | taxonomy: ref_mOTU_3.0.1 meta_mOTU_3.0.1
   # call: python /nile/DB/milanese/CONDA/miniconda3.9/bin/../share/motus-3.0.1//motus profile -f filtered_1P -r filtered_2P -s filtered_1U,filtered_2U -o test_sample.motus -t 8
   #consensus_taxonomy	unnamed sample
   Leptospira alexanderi [ref_mOTU_v3_00001]	0.0000000000
   Leptospira weilii [ref_mOTU_v3_00002]	0.0000000000
   Chryseobacterium sp. [ref_mOTU_v3_00004]	0.0000000000
   Chryseobacterium gallinarum [ref_mOTU_v3_00005]	0.0000000000
   Chryseobacterium indologenes [ref_mOTU_v3_00006]	0.0000000000
   Chryseobacterium artocarpi/ureilyticum [ref_mOTU_v3_00007]	0.0000000000
   Chryseobacterium jejuense [ref_mOTU_v3_00008]	0.0000000000
   ```
   In the profile, `ref_mOTU` represent species with a reference genome sequence in NCBI. While `meta_mOTU` and `ext_mOTU` represent species that have not been sequenced yet (they do not have a representative genome in NCBI).
   the `unassigned` at the end of the file (check with `tail test_sample.motus`), represents species that we know to be present, but are not able to profile. The `unassigned` is useful to have a correct evaluation of the relative abundance of all species (more info [here](https://github.com/motu-tool/mOTUs_v2/wiki/Explain-the-resulting-profile#-1)).
   </p> 

2. How many species are detected? How many are reference species and how many are unknown species?
   <details><summary>SOLUTION</summary>
   <p>

   We can have a look at the identified species that are different from zero with `cat test_sample.motus | grep -v "0.0000000000"`:
   ```
   # git tag version 3.0.1 |  motus version 3.0.1 | map_tax 3.0.1 | gene database: nr3.0.1 | calc_mgc 3.0.1 -y insert.scaled_counts -l 75 | calc_motu 3.0.1 -k mOTU -C no_CAMI -g 3 | taxonomy: ref_mOTU_3.0.1 meta_mOTU_3.0.1
   # call: python /nile/DB/milanese/CONDA/miniconda3.9/bin/../share/motus-3.0.1//motus profile -f filtered_1P -r filtered_2P -s filtered_1U,filtered_2U -o test_sample.motus -t 8
   #consensus_taxonomy	unnamed sample
   Escherichia coli [ref_mOTU_v3_00095]	0.0001797485
   Eggerthella lenta [ref_mOTU_v3_00719]	0.0001633324
   Ruminococcus bromii [ref_mOTU_v3_00853]	0.0049443856
   ```
   
   With `cat test_sample.motus | grep -v "0.0000000000" | grep -v "#" | wc -l`, we can see that we have 158 detected species.
   
   To count the number of ref-mOTUs you can use:
   ```
   cat test_sample.motus | grep -v "0.0000000000" | grep -c "ref_mOTU_"
   ```
   and for meta-mOTUs:
   ```
   cat test_sample.motus | grep -v "0.0000000000" | grep -c "meta_mOTU_"
   ```
   and for ext-mOTUs:
   ```
   cat test_sample.motus | grep -v "0.0000000000" | grep -c "ext_mOTU_"
   ```
   </p> 


3. Can you change some parameters in `motus` to profile more or less species? 
   <details><summary>SOLUTION</summary>
   <p>

   In the mOTUs wiki page there are more information: [link](https://github.com/motu-tool/mOTUs_v2/wiki/Increase-precision-or-recall).  
   Basically, you can use `-g` and `-l` to increase(/decrease) the specificity of the detected species. Lower values allows you to profile more species (lax thresholds); while higher values corresponds to stringent cutoffs (and hence less profiled species).  

   If we run:
   ```
   motus profile -f filtered_1P -r filtered_2P -s filtered_1U,filtered_2U -o test_sample_relax.motus -t 8 -g 1 -l 40 -n relax
   cat test_sample_relax.motus | grep -v "0.0000000000" | grep -v "#" | wc -l
   ```
   We obtain 356 species (198 more species than the default parameters). While, with:
   ```
   motus profile -f filtered_1P -r filtered_2P -s filtered_1U,filtered_2U -o test_sample_stringent.motus -t 8 -g 6 -l 90 -n stringent
   cat test_sample_stringent.motus | grep -v "0.0000000000" | grep -v "#" | wc -l
   ```
   we profile only 91 species (67 less than using the standard parameters).
   
   If you don't manage to run them (~4 mins each), you can download the profiles with:
   ```
   wget https://www.embl.de/download/zeller/metaG_course_2021/test_sample.motus
   wget https://www.embl.de/download/zeller/metaG_course_2021/test_sample_relax.motus
   wget https://www.embl.de/download/zeller/metaG_course_2021/test_sample_stringent.motus
   ```
   </p> 


4. How can you merge different motus profiles into one file?
   Try to merge the three files created in the previous exercises into one profile.
   
   <details><summary>SOLUTION</summary>
   <p>

   You can use `motus merge` (more info [here](https://github.com/motu-tool/mOTUs_v2/wiki/Merge-profiles-into-a-table)).  
   Note that the name of the samples is speciefied by `-n` when using `motus profile`. 
   
   You can run:
   ```
   motus merge -i test_sample.motus,test_sample_relax.motus,test_sample_stringent.motus -o all_samples.motus
   ```
   which looks like (`head all_samples.motus`):
   ```
   # motus version 3.0.0 | merge 3.0.0 | info merged profiles: # git tag version 3.0.1 |  motus version 3.0.1 | map_tax 3.0.1 | gene database: nr3.0.1 | calc_mgc 3.0.1 -y insert.scaled_counts -l 40 | calc_motu 3.0.1 -k mOTU -C no_CAMI -g 1 | taxonomy: ref_mOTU_3.0.1 meta_mOTU_3.0.1 # git tag version 3.0.1 |  motus version 3.0.1 | map_tax 3.0.1 | gene database: nr3.0.1 | calc_mgc 3.0.1 -y insert.scaled_counts -l 75 | calc_motu 3.0.1 -k mOTU -C no_CAMI -g 3 | taxonomy: ref_mOTU_3.0.1 meta_mOTU_3.0.1 # git tag version 3.0.1 |  motus version 3.0.1 | map_tax 3.0.1 | gene database: nr3.0.1 | calc_mgc 3.0.1 -y insert.scaled_counts -l 90 | calc_motu 3.0.1 -k mOTU -C no_CAMI -g 6 | taxonomy: ref_mOTU_3.0.1 meta_mOTU_3.0.1 
   # call: python motus merge -i test_sample.motus,test_sample_relax.motus,test_sample_stringent.motus -o all_samples.motus
   #consensus_taxonomy	test_sample	relax	stringent
   Leptospira alexanderi [ref_mOTU_v3_00001]	0.0000000000	0.0000000000	0.0000000000
   Leptospira weilii [ref_mOTU_v3_00002]	0.0000000000	0.0000000000	0.0000000000
   Chryseobacterium sp. [ref_mOTU_v3_00004]	0.0000000000	0.0000000000	0.0000000000
   Chryseobacterium gallinarum [ref_mOTU_v3_00005]	0.0000000000	0.0000000000	0.0000000000
   Chryseobacterium indologenes [ref_mOTU_v3_00006]	0.0000000000	0.0000000000	0.0000000000
   Chryseobacterium artocarpi/ureilyticum [ref_mOTU_v3_00007]	0.0000000000	0.0000000000	0.0000000000
   Chryseobacterium jejuense [ref_mOTU_v3_00008]	0.0000000000	0.0000000000	0.0000000000
   ```

   We can also check the one that are all different from zero by running:
   ```
   cat all_samples.motus |  grep -vP "0.0000000000\t0.0000000000\t0.0000000000"
   ```


   Note that that if you want to load this table into another tool (for example R), you would like to skip the first 2 rows.
   
   </p> 
   
5. (bonus) Profile the unfiltered fastq files from the test sample and compare them to the filtered one. 
   <details><summary>SOLUTION</summary>
   <p>

   We can profile the unfiltered reads with:
   ```
   motus profile -f raw_reads_1.fastq -r raw_reads_2.fastq -t 8 -n unfiltered -o unfiltered.motus
   ```
   
   Let's have a look at the species that are profiled by both:
   ```
   motus merge -i unfiltered.motus,test_sample.motus | grep -v "0.0000000000" | grep -v "#" | head
   ```
   Which results in:
   ```
   Escherichia coli [ref_mOTU_v3_00095]	0.0002054152	0.0001797485
   Eggerthella lenta [ref_mOTU_v3_00719]	0.0001531478	0.0001633324
   Ruminococcus bromii [ref_mOTU_v3_00853]	0.0046285150	0.0049443856
   Bacteroides uniformis [ref_mOTU_v3_00855]	0.0179185757	0.0188356677
   Anaerostipes hadrus [ref_mOTU_v3_00857]	0.0000810850	0.0000864773
   Roseburia faecis [ref_mOTU_v3_00859]	0.0003944823	0.0004207160
   Roseburia inulinivorans [ref_mOTU_v3_00860]	0.0001580503	0.0001690509
   Roseburia hominis [ref_mOTU_v3_00861]	0.0004800660	0.0005119912
   Bifidobacterium longum [ref_mOTU_v3_01099]	0.0023339573	0.0027482230
   Megasphaera elsdenii [ref_mOTU_v3_01516]	0.0006391750	0.0006816812
   ```

   The two profiles are really similar. mOTUs is filtering the reads internally based on how good they map to the marker gene sequences; hence trimming and filtering the reads before will not affect much the profiles. For other analysis (like building metagenome-assembled genomes) trimming the reads improve the result.
   </p> 
   


  
6. (bonus) Download this metagenomic sample:

   - https://www.embl.de/download/zeller/metaG_course_2021/human_gut_sample_for.fq
   - https://www.embl.de/download/zeller/metaG_course_2021/human_gut_sample_rev.fq

   and profile with mOTUs3 and MetaPhlAn3 (already installed and can be run with `metaphlan`, manual: [link](https://github.com/biobakery/MetaPhlAn/wiki/MetaPhlAn-3.0#basic-usage)). How many species are profiled by the tools? How many different families?

   Since MetaPhlaAn has to download the database first, if there is no time we can download the profiles with:
   ```
   wget https://www.embl.de/download/zeller/metaG_course_2021/human_gut.motus
   wget https://www.embl.de/download/zeller/metaG_course_2021/human_gut.metaphlan
   ```



   <details><summary>SOLUTION</summary>
   <p>

   We can download the samples with:
   ```
   wget https://www.embl.de/download/zeller/metaG_course_2021/human_gut_sample_for.fq
   wget https://www.embl.de/download/zeller/metaG_course_2021/human_gut_sample_rev.fq
   ```

   We can run motus with:
   ```
   motus profile -f human_gut_sample_for.fq -r human_gut_sample_rev.fq -o human_gut.motus -t 8 -A
   ```

   We can run metaphlan with:
   ```
   metaphlan --input_type fastq --nproc 16 --bowtie2out human_gut.bowtie2.bz2 -o human_gut.metaphlan human_gut_sample_for.fq,human_gut_sample_rev.fq
   ```

   We can have a look at the result. The `-A` in mOTUs change the result (`head human_gut.motus`):
   ```
   #mOTUs_clade	unnamed sample
   k__Bacteria	0.9481725845
   k__Bacteria|p__Proteobacteria	0.0089415128
   k__Bacteria|p__Firmicutes	0.3280255558
   k__Bacteria|p__Actinobacteria	0.0533709217
   k__Bacteria|p__Fusobacteria	0.0439658758
   k__Bacteria|p__Bacteroidetes	0.5136232781
   k__Bacteria|p__Bacteria phylum incertae sedis	0.0002454402
   k__Bacteria|p__Proteobacteria|c__Gammaproteobacteria	0.0078059168
   k__Bacteria|p__Proteobacteria|c__Deltaproteobacteria	0.0011355960
   ```

   How many species there are for motus:
   ```
   cat human_gut.motus | grep "s__" | wc -l
   ```
   There are 122 species measured

   How many species there are for metaphlan:
   ```
   cat human_gut.metaphlan | grep "s__" | wc -l
   ```
   There are 48 species measured

   How many families there are for motus:
   ```
   cat human_gut.motus | grep "f__" | grep -v "g__" | wc -l
   ```
   There are 32 families measured

   How many families there are for metaphlan:
   ```
   cat human_gut.metaphlan | grep "f__" | grep -v "g__" | wc -l
   ```
   There are 16 families measured


   </p> 
   
